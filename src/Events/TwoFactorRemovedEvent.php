<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Events;

use MiniatureHappiness\CoreBundle\Interfaces\EventInterface;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;

class TwoFactorRemovedEvent implements EventInterface
{
    public static string $NAME = 'miniature-happiness.core-bundle.event.two-factor-removed';

    private UserInterface $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public function getName(): string
    {
        return self::$NAME;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }
}