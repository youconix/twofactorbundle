<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Events;

use MiniatureHappiness\CoreBundle\Auth\TwoFactor\TwoFactorInterface;
use MiniatureHappiness\CoreBundle\Interfaces\EventInterface;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;

class TwoFactorAddedEvent implements EventInterface
{
    public static string $NAME = 'miniature-happiness.core-bundle.event.two-factor-added';

    private TwoFactorInterface $twoFactor;

    public function __construct(TwoFactorInterface $twoFactor)
    {
        $this->twoFactor = $twoFactor;
    }

    public function getName(): string
    {
        return self::$NAME;
    }

    public function getUser(): UserInterface
    {
        return $this->twoFactor->getUser();
    }

    public function getTwoFactor(): TwoFactorInterface
    {
        return $this->twoFactor;
    }
}