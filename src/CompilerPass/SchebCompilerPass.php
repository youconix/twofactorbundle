<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SchebCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $schebConfiguration = $container->getExtensionConfig('scheb_two_factor');

        if (true) {
            $this->email($container);
        }

        if (true) {
            $this->totp($container);
        }

        if (true) {
            $this->googleAuthenticator($container);
        }

        if (true) {
            $this->authy($container);
        }

        print_r($schebConfiguration);
    }

    private function email(ContainerBuilder $container): void
    {
        $container->getDefinition('miniature-happiness.two-factor.router.email')
            ->addTag('miniature-happiness.two-factor.retriever');

        $enabled = $container->getParameter('miniature-happiness.two-factor.email.enabled');
        if ($enabled) {
            $container->getDefinition('miniature-happiness.two-factor.router.email')
                ->replaceArgument(
                    '$url',
                    $container->getParameter('miniature-happiness.two-factor.email.url')
                )
                ->addTag('miniature-happiness.two-factor.router');
        }
    }

    private function totp(ContainerBuilder $container): void
    {
        $container->getDefinition('miniature-happiness.two-factor.router.totp')
            ->addTag('miniature-happiness.two-factor.retriever');

        $enabled = $container->getParameter('miniature-happiness.two-factor.totp.enabled');
        if ($enabled) {
            $container->getDefinition('miniature-happiness.two-factor.router.totp')
                ->replaceArgument(
                    '$url',
                    $container->getParameter('miniature-happiness.two-factor.totp.url')
                )
                ->replaceArgument(
                    '$qrUrl',
                    $container->getParameter('miniature-happiness.two-factor.totp.url_qr')
                )
                ->addTag('miniature-happiness.two-factor.router');
        }
    }

    private function googleAuthenticator(ContainerBuilder $container): void
    {
        $container->getDefinition('miniature-happiness.two-factor.router.google_authenticator')
            ->addTag('miniature-happiness.two-factor.retriever');

        $enabled = $container->getParameter('miniature-happiness.two-factor.ga.enabled');
        if ($enabled) {
            $container->getDefinition('miniature-happiness.two-factor.router.google_authenticator')
                ->replaceArgument(
                    '$url',
                    $container->getParameter('miniature-happiness.two-factor.ga.url')
                )
                ->replaceArgument(
                    '$qrUrl',
                    $container->getParameter('miniature-happiness.two-factor.ga.url_qr')
                )
                ->addTag('miniature-happiness.two-factor.router');
        }
    }

    private function authy(ContainerBuilder $container): void
    {
        $container->getDefinition('miniature-happiness.two-factor.router.authy')
            ->addTag('miniature-happiness.two-factor.retriever');

        $enabled = $container->getParameter('miniature-happiness.two-factor.authy.enabled');
        if ($enabled) {
            $container->getDefinition('miniature-happiness.two-factor.router.authy')
                ->replaceArgument(
                    '$url',
                    $container->getParameter('miniature-happiness.two-factor.authy.url')
                )
                ->addTag('miniature-happiness.two-factor.router');
        }
    }
}