<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Routing;

use MiniatureHappiness\TwoFactorBundle\Controller\TotpSetupController;
use MiniatureHappiness\TwoFactorBundle\Interfaces\TwoFactorRoutingInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Totp implements TwoFactorRoutingInterface
{
    private string $url;
    private string $qrUrl;

    public function __construct(string $url, string $qrUrl)
    {
        $this->url = $url;
        $this->qrUrl = $qrUrl;
    }

    public function build(RouteCollection $collection): void
    {
        $collection->add(
            'miniature-happiness.two-factor.totp',
            $this->createRoute($this->url, 'index', 'GET')
        );
        $collection->add(
            'miniature-happiness.two-factor.totp.qr',
            $this->createRoute($this->qrUrl, 'qrCode', 'POST')
        );
        $collection->add(
            'miniature-happiness.two-factor.totp_submit',
            $this->createRoute($this->url, 'setup', 'POST')
        );
    }

    private function createRoute(string $url, string $action, string $method): Route
    {
        return (new Route($url))
            ->setDefault('controller', sprintf('%s::%s', TotpSetupController::class, $action))
            ->setMethods($method);
    }
}