<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Routing;

use MiniatureHappiness\TwoFactorBundle\Controller\GoogleAuthenticatorSetupController;
use MiniatureHappiness\TwoFactorBundle\Interfaces\TwoFactorRoutingInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class GoogleAuthenticator implements TwoFactorRoutingInterface
{
    private string $url;
    private string $qrUrl;

    public function __construct(string $url, string $qrUrl)
    {
        $this->url = $url;
        $this->qrUrl = $qrUrl;
    }

    public function build(RouteCollection $collection): void
    {
        $collection->add(
            'miniature-happiness.two-factor.ga',
            $this->createRoute($this->url, 'index', 'GET')
        );
        $collection->add(
            'miniature-happiness.two-factor.ga.qr',
            $this->createRoute($this->qrUrl, 'qrCode', 'POST')
        );
        $collection->add(
            'miniature-happiness.two-factor.ga_submit',
            $this->createRoute($this->url, 'setup', 'POST')
        );
    }

    private function createRoute(string $url, string $action, string $method): Route
    {
        return (new Route($url))
            ->setDefault('controller', sprintf('%s::%s', GoogleAuthenticatorSetupController::class, $action))
            ->setMethods($method);
    }
}