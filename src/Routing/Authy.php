<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Routing;

use MiniatureHappiness\TwoFactorBundle\Controller\AuthySetupController;
use MiniatureHappiness\TwoFactorBundle\Interfaces\TwoFactorRoutingInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Authy implements TwoFactorRoutingInterface
{
    private string $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function build(RouteCollection $collection): void
    {
        $collection->add(
            'miniature-happiness.two-factor.authy',
            $this->createRoute($this->url, 'index', 'GET')
        );
        $collection->add(
            'miniature-happiness.two-factor.authy_submit',
            $this->createRoute($this->url, 'toggle', 'POST')
        );
    }

    private function createRoute(string $url, string $action, string $method): Route
    {
        return (new Route($url))
            ->setDefault('controller', sprintf('%s::%s', AuthySetupController::class, $action))
            ->setMethods($method);
    }
}