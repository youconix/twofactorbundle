<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Routing;

use IteratorAggregate;
use MiniatureHappiness\TwoFactorBundle\Interfaces\TwoFactorRoutingInterface;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\RouteCollection;

class Router extends Loader
{
    /**
     * @var IteratorAggregate<TwoFactorRoutingInterface>
     */
    private IteratorAggregate $routes;
    private bool $isLoaded = false;

    /**
     * @param IteratorAggregate<TwoFactorRoutingInterface> $routes
     */
    public function __construct(IteratorAggregate $routes, string $env = null)
    {
        parent::__construct($env);

        $this->routes = $routes;
    }

    public function load($resource, string $type = null): RouteCollection
    {
        if (true === $this->isLoaded) {
            throw new \RuntimeException('Do not add the "extra" loader twice');
        }
        $this->isLoaded = true;

        $routes = new RouteCollection();

        foreach($this->routes as $route) {
            $route->build($routes);
        }

        return $routes;
    }

    public function supports($resource, string $type = null): bool
    {
        return $type === 'miniature-happiness-routes';
    }
}