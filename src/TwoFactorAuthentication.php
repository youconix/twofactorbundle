<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle;

use Doctrine\ORM\EntityManagerInterface;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;
use MiniatureHappiness\CoreBundle\Traits\FingerprintTrait;
use MiniatureHappiness\TwoFactorBundle\Events\TwoFactorAddedEvent;
use MiniatureHappiness\TwoFactorBundle\Events\TwoFactorRemovedEvent;
use RuntimeException;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class TwoFactorAuthentication
{
    use FingerprintTrait;

    private EntityManagerInterface $entityManager;
    private Request $request;
    private EventDispatcher $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack, EventDispatcher $eventDispatcher)
    {
        $request = $requestStack->getMainRequest();
        if ($request === null) {
            throw new RuntimeException('Missing request.');
        }

        $this->entityManager = $entityManager;
        $this->request = $request;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function has2FA(UserInterface $user): bool
    {
        return false;
    }

    public function set2fa(UserInterface $user): void
    {
        $this->removeSessions($user);

        // Generate new session key
        $sessionKey = random_bytes(40);
        $this->request->cookies->set('auth', $sessionKey);

        $session = new TwoFactorSession(
            (int) $user->getUserid(),
            $this->getFingerprint(),
            $sessionKey,
            time() + 24 * 60 * 60
        );

        $this->entityManager->persist($session);

        $event = new TwoFactorAddedEvent($user);
        $this->eventDispatcher->dispatch($event, $event->getName());
    }

    public function destroySessions(UserInterface $user): void
    {
        $this->removeSessions($user);

        $event = new TwoFactorRemovedEvent($user);
        $this->eventDispatcher->dispatch($event, $event->getName());
    }

    private function removeSessions(UserInterface $user): void
    {
        $sessionKey = $this->request->cookies->get('auth', '');

        $this->entityManager
            ->createQueryBuilder()
            ->delete(TwoFactorSession::class, 'tw')
            ->where('tw.user_id = :user_id')
            ->andWhere('tw.session_key = :session_key')
            ->setParameter('user_id', (int) $user->getUserid())
            ->setParameter('session_key', $sessionKey)
            ->getQuery()
            ->execute();
    }
}