<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="two_factor_trusted_devices")
 */
class TrustedDevices
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length="100")
     */
    private string $fingerprint;

    /**
     * @ORM\ManyToOne(targetEntity="MiniatureHappiness\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private UserInterface $user;

    /**
     * @ORM\Column(type="datetime", name="valid_until")
     */
    private DateTimeInterface $validUntil;

    public function __construct(string $fingerprint, UserInterface $user, DateTimeInterface $validUntil)
    {
        $this->fingerprint = $fingerprint;
        $this->user = $user;
        $this->validUntil = $validUntil;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFingerprint(): string
    {
        return $this->fingerprint;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function getValidUntil(): DateTimeInterface
    {
        return $this->validUntil;
    }

    /**
     * @return $this
     */
    public function setFingerprint(string $fingerprint): static
    {
        $this->fingerprint = $fingerprint;
        return $this;
    }

    /**
     * @return $this
     */
    public function setUser(UserInterface $user): static
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return $this
     */
    public function setValidUntil(DateTimeInterface $validUntil): static
    {
        $this->validUntil = $validUntil;
        return $this;
    }
}