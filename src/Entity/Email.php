<?php

namespace MiniatureHappiness\TwoFactorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MiniatureHappiness\TwoFactorBundle\Interfaces\EmailInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="two_factor_email")
 */
class Email extends TwoFactor implements EmailInterface
{
    public const TYPE = 'two_factor_email';

    /**
     * @ORM\Column(type="string", length="100", name="auth_code")
     */
    private ?string $authCode = null;

    public function getEmailAuthCode(): ?string
    {
        return $this->authCode;
    }

    public function setEmailAuthCode(string $authCode): void
    {
        $this->authCode = $authCode;
    }

    public function isEmailAuthEnabled(): bool
    {
        return $this->isEnabled();
    }

    public function getEmailAuthRecipient(): string
    {
        return $this->user->getEmail();
    }

    protected function getType(): string
    {
        return self::TYPE;
    }
}