<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;
use MiniatureHappiness\TwoFactorBundle\Interfaces\GoogleAuthenticatorInterface;
use MiniatureHappiness\TwoFactorBundle\Totp\TotpConfiguration;

/**
 * @ORM\Entity()
 * @ORM\Table(name="two_factor_google_authenticator")
 */
class GoogleAuthenticator extends TwoFactor implements GoogleAuthenticatorInterface
{
    public const TYPE = '';

    /**
     * @ORM\Column(type="integer")
     */
    protected int $length;

    /**
     * @ORM\Column(type="string", length="160")
     */
    protected string $secret;

    public function __construct(UserInterface $user, string $secret)
    {
        parent::__construct($user);

        $this->secret = $secret;
    }

    public function getAlgorithm(): string
    {
        return TotpConfiguration::ALGORITHM_SHA1;
    }

    public function getGoogleAuthenticatorUsername(): string
    {
        return $this->user->getUsername();
    }

    public function getGoogleAuthenticatorSecret(): ?string
    {
        return $this->secret;
    }

    public function getLength(): int
    {
        return 6;
    }

    public function isGoogleAuthenticatorEnabled(): bool
    {
        return $this->isEnabled();
    }

    /**
     * @return $this
     */
    public function setSecret(string $secret): static
    {
        $this->secret = $secret;
        return $this;
    }

    protected function getType(): string
    {
        return self::TYPE;
    }
}