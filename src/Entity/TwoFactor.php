<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MiniatureHappiness\CoreBundle\Auth\TwoFactor\TwoFactorInterface;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="two_factor")
 * @ORM\InheritanceType('JOINED')
 * @ORM\DiscriminatorColumn(name: 'type', type: 'string')
 * @ORM\DiscriminatorMap([
 *     'email' => MiniatureHappiness\CoreBundle\Auth\TwoFactor\Entity\Email,
 *     'totp' => MiniatureHappiness\CoreBundle\Auth\TwoFactor\Entity\Totp,
 *     'google' => MiniatureHappiness\CoreBundle\Auth\TwoFactor\Entity\GoogleAuthenticator,
 *     'authy' => MiniatureHappiness\CoreBundle\Auth\TwoFactor\Entity\Authy,
 * ])
 */
abstract class TwoFactor implements TwoFactorInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(type="string", length="100")
     */
    protected string $name;

    /**
     * @ORM\OneToOne(targetEntity="MiniatureHappiness\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected UserInterface $user;

    /**
     * @ORM\Column(type="boolean", name="is_enabled")
     */
    private bool $isEnabled;

    public function __construct(UserInterface $user)
    {
        $this->name = $this->getType();
        $this->user = $user;
        $this->isEnabled = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function setEnabled(bool $isEnabled): static
    {
        $this->isEnabled = $isEnabled;
        return $this;
    }

    public function setUser(UserInterface $user): static
    {
        $this->user = $user;
        return $this;
    }

    abstract protected function getType(): string;
}