<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="two_factor_authy")
 */
class Authy extends TwoFactor
{
    public const TYPE = 'two_factor_authy';

    /**
     * @ORM\Column(type="string", length="30")
     */
    private string $code = '';

    /**
     * @ORM\Column(type="string", length="20")
     */
    private string $telephone = '';

    public function getCode(): string
    {
        return $this->code;
    }

    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @return $this
     */
    public function setCode(string $code): static
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return $this
     */
    public function setTelephone(string $telephone): static
    {
        $this->telephone = $telephone;
        return $this;
    }

    protected function getType(): string
    {
        return self::TYPE;
    }
}