<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;
use MiniatureHappiness\TwoFactorBundle\Interfaces\TotpInterface;
use MiniatureHappiness\TwoFactorBundle\Interfaces\TotpConfigurationInterface;
use MiniatureHappiness\TwoFactorBundle\Totp\TotpConfiguration;

/**
 * @ORM\Entity()
 * @ORM\Table(name="two_factor_totp")
 */
class Totp extends TwoFactor implements TotpInterface
{
    public const TYPE = 'two_factor_totp';

    /**
     * @ORM\Column(type="string", length="160")
     */
    protected string $algorithm;

    /**
     * @ORM\Column(type="integer")
     */
    protected int $length;

    /**
     * @ORM\Column(type="string", length="160")
     */
    protected string $secret;

    public function __construct(UserInterface $user, string $secret, string $algorithm, int $length = 8)
    {
        parent::__construct($user);

        $this->algorithm = $algorithm;
        $this->secret = $secret;
        $this->length = $length;
    }

    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    public function getTotpAuthenticationConfiguration(): ?TotpConfigurationInterface
    {
        return new TotpConfiguration($this->secret, $this->algorithm, 20, $this->length);
    }

    public function getTotpAuthenticationUsername(): string
    {
        return $this->user->getUsername();
    }

    public function isTotpAuthenticationEnabled(): bool
    {
        return $this->isEnabled();
    }

    /**
     * @return $this
     */
    public function setLength(int $length): static
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return $this
     */
    public function setAlgorithm(string $algorithm): static
    {
        $this->algorithm = $algorithm;
        return $this;
    }

    /**
     * @return $this
     */
    public function setSecret(string $secret): static
    {
        $this->secret = $secret;
        return $this;
    }

    protected function getType(): string
    {
        return self::TYPE;
    }
}