<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="two_factor_backup_codes")
 */
class BackupCode
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length="100")
     */
    private string $code;

    /**
     * @ORM\ManyToOne(targetEntity="MiniatureHappiness\CoreBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private UserInterface $user;

    public function __construct(string $code, UserInterface $user)
    {
        $this->code = $code;
        $this->user = $user;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @return $this
     */
    public function setCode(string $code): static
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return $this
     */
    public function setUser(UserInterface $user): static
    {
        $this->user = $user;
        return $this;
    }
}