<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Retrievers;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use MiniatureHappiness\CoreBundle\Auth\TwoFactor\TwoFactorInterface;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;
use MiniatureHappiness\TwoFactorBundle\Entity\Totp;
use MiniatureHappiness\TwoFactorBundle\Events\TwoFactorAddedEvent;
use MiniatureHappiness\TwoFactorBundle\Events\TwoFactorRemovedEvent;
use MiniatureHappiness\TwoFactorBundle\Interfaces\TwoFactorRetrieverInterface;
use MiniatureHappiness\TwoFactorBundle\Totp\TotpConfiguration;
use Symfony\Component\EventDispatcher\EventDispatcher;

class TotpRetriever implements TwoFactorRetrieverInterface
{
    private EventDispatcher $eventDispatcher;
    private EntityManagerInterface $entityManager;
    private int $length = 8;
    private string $algorithm = TotpConfiguration::ALGORITHM_SHA1;

    public function __construct(EventDispatcher $eventDispatcher, EntityManagerInterface $entityManager, $length = 8)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
        $this->length = $length;
    }

    /**
     * @return ?Totp
     * @throws NonUniqueResultException
     */
    public function getForUser(UserInterface $user): ?TwoFactorInterface
    {
        return $this->entityManager->createQueryBuilder()
            ->from(Totp::class, 'a')
            ->where('a.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function createForUser(UserInterface $user): Totp
    {
        $twoFactor = $this->getForUser($user);
        $secret = $this->generateSecret();

        if ($twoFactor === null) {
            $twoFactor = new Totp($user, $secret, $this->algorithm, $this->length);
        }
        else {
            $twoFactor
                ->setSecret($secret)
                ->setEnabled(false);
        }

        $this->save($twoFactor);

        return $twoFactor;
    }

    public function save(TwoFactorInterface $twoFactor): static
    {
        $this->entityManager->persist($twoFactor);
    }

    public function remove(TwoFactorInterface $twoFactor): static
    {
        $this->entityManager->remove($twoFactor);
        $this->entityManager->flush();

        $this->eventDispatcher->dispatch(
            new TwoFactorRemovedEvent($twoFactor),
            TwoFactorRemovedEvent::$NAME
        );
    }

    public function enable(TwoFactorInterface $twoFactor): static
    {
        $twoFactor->setEnabled(true);
        $this->save($twoFactor);
        $this->entityManager->flush();

        $this->eventDispatcher->dispatch(
            new TwoFactorAddedEvent($twoFactor),
            TwoFactorAddedEvent::$NAME
        );
    }

    public function supports(string $type): bool
    {
        return $type === Totp::TYPE;
    }

    private function generateSecret(): string
    {
    }
}