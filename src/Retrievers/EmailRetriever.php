<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Retrievers;

use Doctrine\ORM\NonUniqueResultException;
use MiniatureHappiness\CoreBundle\Auth\TwoFactor\TwoFactorInterface;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;
use MiniatureHappiness\TwoFactorBundle\Entity\Email;

class EmailRetriever extends AbstractRetriever
{
    /**
     * @throws NonUniqueResultException
     */
    public function getForUser(UserInterface $user): ?TwoFactorInterface
    {
        return $this->entityManager->createQueryBuilder()
            ->from(Email::class, 'a')
            ->where('a.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function createForUser(UserInterface $user): Email
    {
        $twoFactor = $this->getForUser($user);
        if ($twoFactor === null) {
            $twoFactor = new Email($user);
        }
        $twoFactor
            ->setEnabled(true)
            ->setEmailAuthCode('');

        $this->save($twoFactor);

        return $twoFactor;
    }

    public function supports(string $type): bool
    {
        return $type === Email::TYPE;
    }
}