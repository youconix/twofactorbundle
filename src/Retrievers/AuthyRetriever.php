<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Retrievers;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use MiniatureHappiness\CoreBundle\Auth\TwoFactor\TwoFactorInterface;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;
use MiniatureHappiness\TwoFactorBundle\Entity\Authy;
use MiniatureHappiness\TwoFactorBundle\Events\TwoFactorAddedEvent;
use MiniatureHappiness\TwoFactorBundle\Events\TwoFactorRemovedEvent;
use MiniatureHappiness\TwoFactorBundle\Interfaces\TwoFactorRetrieverInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class AuthyRetriever implements TwoFactorRetrieverInterface
{
    private EventDispatcher $eventDispatcher;
    private EntityManagerInterface $entityManager;

    public function __construct(EventDispatcher $eventDispatcher, EntityManagerInterface $entityManager)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getForUser(UserInterface $user): ?TwoFactorInterface
    {
        return $this->entityManager->createQueryBuilder()
            ->from(Authy::class, 'a')
            ->where('a.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function createForUser(UserInterface $user, string $telephone): Authy
    {
        $twoFactor = $this->getForUser($user);
        if ($twoFactor === null) {
            $twoFactor = new Authy($user);
        }
        $twoFactor
            ->setTelephone($telephone)
            ->setCode('')
            ->setEnabled(false);

        $this->save($twoFactor);

        return $twoFactor;
    }

    public function save(TwoFactorInterface $twoFactor): static
    {
        $this->entityManager->persist($twoFactor);
    }

    public function remove(TwoFactorInterface $twoFactor): static
    {
        $this->entityManager->remove($twoFactor);
        $this->entityManager->flush();

        $this->eventDispatcher->dispatch(
            new TwoFactorRemovedEvent($twoFactor),
            TwoFactorRemovedEvent::$NAME
        );
    }

    /**
     * @return $this
     */
    public function enable(TwoFactorInterface $twoFactor): static
    {
        $twoFactor->setEnabled(true);
        $this->save($twoFactor);
        $this->entityManager->flush();

        $this->eventDispatcher->dispatch(
            new TwoFactorAddedEvent($twoFactor),
            TwoFactorAddedEvent::$NAME
        );
    }

    public function supports(string $type): bool
    {
        return $type === Authy::TYPE;
    }
}