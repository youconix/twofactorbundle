<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Retrievers;

use Doctrine\ORM\EntityManagerInterface;
use MiniatureHappiness\CoreBundle\Auth\TwoFactor\TwoFactorInterface;
use MiniatureHappiness\TwoFactorBundle\Events\TwoFactorAddedEvent;
use MiniatureHappiness\TwoFactorBundle\Events\TwoFactorRemovedEvent;
use MiniatureHappiness\TwoFactorBundle\Interfaces\TwoFactorRetrieverInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

abstract class AbstractRetriever implements TwoFactorRetrieverInterface
{
    protected EventDispatcher $eventDispatcher;
    protected EntityManagerInterface $entityManager;

    public function __construct(EventDispatcher $eventDispatcher, EntityManagerInterface $entityManager)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
    }

    public function save(TwoFactorInterface $twoFactor): static
    {
        $this->entityManager->persist($twoFactor);
    }

    public function remove(TwoFactorInterface $twoFactor): static
    {
        $this->entityManager->remove($twoFactor);
        $this->entityManager->flush();

        $this->eventDispatcher->dispatch(
            new TwoFactorRemovedEvent($twoFactor),
            TwoFactorRemovedEvent::$NAME
        );
    }

    /**
     * @return $this
     */
    public function enable(TwoFactorInterface $twoFactor): static
    {
        $twoFactor->setEnabled(true);
        $this->save($twoFactor);
        $this->entityManager->flush();

        $this->eventDispatcher->dispatch(
            new TwoFactorAddedEvent($twoFactor),
            TwoFactorAddedEvent::$NAME
        );
    }
}