<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Retrievers;

use MiniatureHappiness\CoreBundle\Auth\TwoFactor\TwoFactorInterface;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;
use MiniatureHappiness\TwoFactorBundle\Entity\GoogleAuthenticator;
use MiniatureHappiness\TwoFactorBundle\Interfaces\TwoFactorRetrieverInterface;

class GoogleAuthenticatorRetriever implements TwoFactorRetrieverInterface
{
    /**
     * @return ?GoogleAuthenticator
     */
    public function getForUser(UserInterface $user): ?TwoFactorInterface
    {
        // TODO: Implement getForUser() method.
    }

    public function createForUser(UserInterface $user): GoogleAuthenticator
    {
        $twoFactor = $this->getForUser($user);
        $secret = $this->generateSecret();

        if ($twoFactor === null) {
            $twoFactor = new GoogleAuthenticator($user, $secret);
        } else {
            $twoFactor
                ->setSecret($secret)
                ->setEnabled(false);
        }

        $this->save($twoFactor);

        return $twoFactor;
    }

    public function save(TwoFactorInterface $twoFactor): static
    {
        // TODO: Implement save() method.
    }

    public function remove(TwoFactorInterface $twoFactor): static
    {
        // TODO: Implement remove() method.
    }

    public function supports(string $type): bool
    {
        return $type === GoogleAuthenticator::TYPE;
    }

    private function generateSecret(): string
    {
    }
}