<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle;

use IteratorAggregate;
use MiniatureHappiness\CoreBundle\Auth\TwoFactor\TwoFactorInterface;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;
use MiniatureHappiness\TwoFactorBundle\Interfaces\TwoFactorRetrieverInterface;

class TwoFactorRetriever
{
    /**
     * @var IteratorAggregate<TwoFactorRetrieverInterface>
     */
    private IteratorAggregate $retrievers;

    /**
     * @param IteratorAggregate<TwoFactorRetrieverInterface> $retrievers
     */
    public function __construct(IteratorAggregate $retrievers)
    {
        $this->retrievers = $retrievers;
    }

    /**
     * @return TwoFactorInterface[]
     */
    public function getForUser(UserInterface $user): array
    {
        $twoFactors = [];

        foreach($this->retrievers as $retriever)
        {
            $twoFactor = $retriever->getForUser($user);
            if ($twoFactor !== null) {
                $twoFactors[] = $twoFactor;
            }
        }

        return $twoFactors;
    }

    public function getForUserAndType(UserInterface $user, string $type): ?TwoFactorInterface
    {
        foreach($this->retrievers as $retriever)
        {
            if ($retriever->supports($type)) {
                return $retriever->getForUser($user);
            }
        }
    }
}