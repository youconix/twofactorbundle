<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Controller;

use MiniatureHappiness\TwoFactorBundle\Entity\Authy;
use MiniatureHappiness\TwoFactorBundle\Retrievers\AuthyRetriever;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthySetupController extends AbstractController
{
    private AuthyRetriever $retriever;
    private string $templateDisabled;
    private string $templateTelephone;
    private string $templateEnabled;

    public function __construct(AuthyRetriever $retriever, string $templateDisabled, string $templateTelephone, string $templateEnabled)
    {
        $this->retriever = $retriever;
        $this->templateDisabled = $templateDisabled;
        $this->templateTelephone = $templateTelephone;
        $this->templateEnabled = $templateEnabled;
    }

    public function index(): Response
    {
        $twoFactor = $this->getTwoFactor();

        if ($twoFactor === null || !$twoFactor->isEnabled()) {
            return $this->authyDisabled();
        }

        return $this->authyEnabled($twoFactor);
    }

    public function telephone(): Response
    {
        return $this->render($this->templateTelephone);
    }

    public function setup(Request $request): RedirectResponse
    {
        $user = $this->getUser();

        $twoFactor = $this->retriever->createForUser(
            $user,
            $request->request->get('telephone', '')
        );

        $this->retriever->save($twoFactor);

        return $this->redirectToRoute('miniature-happiness.two-factor.router.authy');
    }

    private function authyEnabled(Authy $twoFactor): Response
    {
        return $this->render($this->templateEnabled, ['twoFactor' => $twoFactor]);
    }

    private function authyDisabled(): Response
    {
        return $this->render($this->templateDisabled);
    }
}