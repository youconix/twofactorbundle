<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Controller;

use MiniatureHappiness\CoreBundle\Auth\TwoFactor\TwoFactorInterface;
use MiniatureHappiness\TwoFactorBundle\Retrievers\EmailRetriever;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class EmailSetupController extends AbstractController
{
    private EmailRetriever $retriever;
    private string $templateDisabled;
    private string $templateEnabled;

    public function __construct(EmailRetriever $retriever, string $templateDisabled, string $templateEnabled)
    {
        $this->retriever = $retriever;
        $this->templateDisabled = $templateDisabled;
        $this->templateEnabled = $templateEnabled;
    }

    public function index(): Response
    {
        $twoFactor = $this->getTwoFactor();

        if ($twoFactor === null || !$twoFactor->isEnabled()) {
            return $this->emailDisabled();
        }

        return $this->emailEnabled($twoFactor);
    }

    public function toggle(): RedirectResponse
    {
        $user = $this->getUser();
        $this->retriever->createForUser($user);

        return $this->redirectToRoute('miniature-happiness.two-factor.router.email');
    }

    private function emailEnabled(TwoFactorInterface $twoFactor): Response
    {
        return $this->render($this->templateEnabled, ['twoFactor' => $twoFactor]);
    }

    private function emailDisabled(): Response
    {
        return $this->render($this->templateDisabled);
    }

    private function getTwoFactor(): ?TwoFactorInterface
    {
        $user = $this->getUser();
        return $this->retriever->getForUser($user);
    }
}