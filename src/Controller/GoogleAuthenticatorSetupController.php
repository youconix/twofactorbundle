<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Controller;

use MiniatureHappiness\TwoFactorBundle\Retrievers\GoogleAuthenticatorRetriever;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GoogleAuthenticatorSetupController extends AbstractController
{
    private GoogleAuthenticatorRetriever $retriever;
    private string $templateDisabled;
    private string $templateQr;
    private string $templateEnabled;

    public function __construct(GoogleAuthenticatorRetriever $retriever, string $templateDisabled, string $templateQr, string $templateEnabled)
    {
        $this->retriever = $retriever;
        $this->templateDisabled = $templateDisabled;
        $this->templateQr = $templateQr;
        $this->templateEnabled = $templateEnabled;
    }

    public function index(): Response
    {

    }

    public function qrCode(Request $request): Response
    {

    }

    public function setup(Request $request): RedirectResponse
    {

    }

    private function totpEnabled(): Response
    {

    }

    private function totpDisabled(): Response
    {

    }
}