<?php

namespace MiniatureHappiness\TwoFactorBundle\Interfaces;

use MiniatureHappiness\CoreBundle\Auth\TwoFactor\TwoFactorCollection;
use MiniatureHappiness\CoreBundle\Auth\TwoFactor\TwoFactorInterface;

interface TwoFactorUserInterface
{
    /**
     * @return $this
     */
    public function addTwoFactor(TwoFactorInterface $twoFactor): static;

    public function getTwoFactor(): TwoFactorCollection;
}