<?php

namespace MiniatureHappiness\TwoFactorBundle\Interfaces;

if (interface_exists('\Scheb\TwoFactorBundle\Model\Google\TwoFactorInterface')) {
    interface GoogleAuthenticatorInterface extends \Scheb\TwoFactorBundle\Model\Google\TwoFactorInterface
    {
    }
}
else {
    interface GoogleAuthenticatorInterface
    {
    }
}