<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Interfaces;

use MiniatureHappiness\CoreBundle\Entity\User;

interface TwoFactorHandlerInterface
{
    public function getKey(): string;

    public function isEnabledForUser(User $user): bool;

    public function hasValidSession(User $user, string $fingerprint, string $sessionKey): bool;

    /**
     * @param array<string, string> $parameters
     */
    public function register(User $user, array $parameters): bool;

    /**
     * @param array<string, string> $parameters
     */
    public function startVerification(array $parameters): bool;

    /**
     * @param array<string, string> $parameters
     */
    public function completeVerification(array $parameters): bool;

    public function sendToken(User $user): void;

    public function verifyToken(User $user, string $token): bool;
}