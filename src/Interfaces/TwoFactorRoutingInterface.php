<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Interfaces;

use Symfony\Component\Routing\RouteCollection;

interface TwoFactorRoutingInterface
{
    public function build(RouteCollection $collection): void;
}