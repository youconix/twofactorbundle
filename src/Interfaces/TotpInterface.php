<?php

namespace MiniatureHappiness\TwoFactorBundle\Interfaces;

if (interface_exists('\Scheb\TwoFactorBundle\Model\Totp\TwoFactorInterface')) {
    interface TotpInterface extends \Scheb\TwoFactorBundle\Model\Totp\TwoFactorInterface
    {
    }
}
else {
    interface TotpInterface
    {
    }
}