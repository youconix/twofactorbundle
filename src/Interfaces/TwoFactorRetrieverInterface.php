<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Interfaces;

use MiniatureHappiness\CoreBundle\Auth\TwoFactor\TwoFactorInterface;
use MiniatureHappiness\CoreBundle\Interfaces\UserInterface;

interface TwoFactorRetrieverInterface
{
    public function getForUser(UserInterface $user): ?TwoFactorInterface;

    /**
     * @return $this
     */
    public function save(TwoFactorInterface $twoFactor): static;

    /**
     * @return $this
     */
    public function remove(TwoFactorInterface $twoFactor): static;

    /**
     * @return $this
     */
    public function enable(TwoFactorInterface $twoFactor): static;

    public function supports(string $type): bool;
}