<?php

namespace MiniatureHappiness\TwoFactorBundle\Interfaces;

if (interface_exists('\Scheb\TwoFactorBundle\Model\Email\TwoFactorInterface')) {
    interface EmailInterface extends \Scheb\TwoFactorBundle\Model\Email\TwoFactorInterface
    {
    }
}
else {
    interface EmailInterface
    {
    }
}