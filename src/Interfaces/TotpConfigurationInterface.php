<?php

namespace MiniatureHappiness\TwoFactorBundle\Interfaces;

if (interface_exists('\Scheb\TwoFactorBundle\Model\Totp\TotpConfigurationInterface')){
    interface TotpConfigurationInterface extends \Scheb\TwoFactorBundle\Model\Totp\TotpConfigurationInterface
    {
    }
}
else {
    interface TotpConfigurationInterface
    {
    }
}