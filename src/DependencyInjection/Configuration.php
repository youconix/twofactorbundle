<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\NodeBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('miniature_happiness_two_factor');

        $rootNode = $treeBuilder->getRootNode();

        $builder = $rootNode->addDefaultsIfNotSet()->children();

        $this->email($builder);
        $this->totp($builder);
        $this->googleAuthenticator($builder);
        $this->authy($builder);

        return $treeBuilder;
    }

    private function email(NodeBuilder $builder): void
    {
        $builder->arrayNode('email')
            ->addDefaultsIfNotSet()
            ->children()
                ->booleanNode('enabled')->defaultValue(false)->end()
                ->scalarNode('url')->defaultValue('/2fa/email')->end()
                ->scalarNode('template_disabled')->defaultValue('@MiniatureHappinessTwoFactorBundle/email/disabled.html.twig')->end()
                ->scalarNode('template_enabled')->defaultValue('@MiniatureHappinessTwoFactorBundle/email/enabled.html.twig')->end()
            ->end()
        ->end();
    }

    private function totp(NodeBuilder $builder): void
    {
        $builder->arrayNode('totp')
            ->addDefaultsIfNotSet()
            ->children()
                ->booleanNode('enabled')->defaultValue(false)->end()
                ->scalarNode('url')->defaultValue('/2fa/totp')->end()
                ->scalarNode('url_qr')->defaultValue('/2fa/totp/qr')->end()
                ->scalarNode('template_disabled')->defaultValue('@MiniatureHappinessTwoFactorBundle/totp/disabled.html.twig')->end()
                ->scalarNode('template_qr')->defaultValue('@MiniatureHappinessTwoFactorBundle/totp/qr.html.twig')->end()
                ->scalarNode('template_enabled')->defaultValue('@MiniatureHappinessTwoFactorBundle/totp/enabled.html.twig')->end()
            ->end()
        ->end();
    }

    private function googleAuthenticator(NodeBuilder $builder): void
    {
        $builder->arrayNode('google_authenticator')
            ->addDefaultsIfNotSet()
            ->children()
                ->booleanNode('enabled')->defaultValue(false)->end()
                ->scalarNode('url')->defaultValue('/2fa/ga')->end()
                ->scalarNode('url_qr')->defaultValue('/2fa/ga/qr')->end()
                ->scalarNode('template_disabled')->defaultValue('@MiniatureHappinessTwoFactorBundle/google_authenticator/disabled.html.twig')->end()
                ->scalarNode('template_qr')->defaultValue('@MiniatureHappinessTwoFactorBundle/google_authenticator/qr.html.twig')->end()
                ->scalarNode('template_enabled')->defaultValue('@MiniatureHappinessTwoFactorBundle/google_authenticator/enabled.html.twig')->end()
            ->end()
        ->end();
    }

    private function authy(NodeBuilder $builder): void
    {
        $builder->arrayNode('authy')
            ->addDefaultsIfNotSet()
            ->children()
                ->booleanNode('enabled')->defaultValue(false)->end()
                ->scalarNode('url')->defaultValue('/2fa/authy')->end()
                ->scalarNode('template_disabled')->defaultValue('@MiniatureHappinessTwoFactorBundle/authy/disabled.html.twig')->end()
                ->scalarNode('template_enabled')->defaultValue('@MiniatureHappinessTwoFactorBundle/authy/enabled.html.twig')->end()
            ->end()
        ->end();
    }
}