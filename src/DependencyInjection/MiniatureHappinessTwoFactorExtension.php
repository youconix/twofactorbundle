<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\DependencyInjection;

use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class MiniatureHappinessTwoFactorExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $bundles = $container->getParameter('kernel.bundles');

        if (!isset($bundles['SchebTwoFactorBundle'])) {
            return;
        }

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../../config')
        );

        $loader->load('packages/services.yaml');
        $loader->load('packages/retrievers.yaml');
        $loader->load('packages/router.yaml');
        $loader->load('packages/controllers.yaml');

        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        $this->setParameters($container, $config);
        $this->setControllers($container, $config);
    }

    /**
     * @throws InvalidArgumentException
     */
    public function prepend(ContainerBuilder $container): void
    {
        $yamlParser = new Yaml();

        $this->doctrine($container, $yamlParser);
        $this->migrations($container, $yamlParser);
        $this->twig($container, $yamlParser);
    }

    /**
     * @throws InvalidArgumentException
     */
    private function doctrine(ContainerBuilder $container, Yaml $parser): void
    {
        $fileName = __DIR__.'/../../config/packages/doctrine.yaml';

        $doctrineConfig = $this->parseYaml($parser, $fileName);
        $existingConfig = $container->getExtensionConfig('doctrine');

        if (isset($existingConfig[0]['orm']['mappings'])) {
            $newMapping = array_merge($existingConfig[0]['orm']['mappings'], $doctrineConfig['doctrine']['orm']['entity_managers']['default']['mappings']);
            $doctrineConfig['doctrine']['orm']['entity_managers']['default']['mappings'] = $newMapping;
        }

        $container->prependExtensionConfig('doctrine', $doctrineConfig['doctrine']);
    }

    private function migrations(ContainerBuilder $container, Yaml $parser): void
    {
        $fileName = __DIR__.'/../../config/packages/doctrine_migrations.yaml';

        $config = $this->parseYaml($parser, $fileName);
        $container->prependExtensionConfig('doctrine_migrations', $config['doctrine_migrations']);
    }

    private function twig(ContainerBuilder $container, Yaml $parser): void
    {
        $fileName = __DIR__.'/../../config/packages/twig.yaml';

        $config = $this->parseYaml($parser, $fileName);
        $container->prependExtensionConfig('twig', $config['twig']);
    }

    /**
     * @throws InvalidArgumentException
     */
    private function parseYaml(Yaml $parser, string $fileName): array
    {
        try {
            return $parser->parse(file_get_contents($fileName));
        } catch (ParseException $e) {
            throw new InvalidArgumentException(sprintf('The file "%s" does not contain valid YAML.', $fileName), 0, $e);
        }
    }

    private function setParameters(ContainerBuilder $container, array $config): void
    {
        print_r($config);
        $container->setParameter('miniature-happiness.two-factor.email.enabled', $config['email']['enabled']);
        $container->setParameter('miniature-happiness.two-factor.email.url', $config['email']['url']);

        $container->setParameter('miniature-happiness.two-factor.totp.enabled', $config['totp']['enabled']);
        $container->setParameter('miniature-happiness.two-factor.totp.url', $config['totp']['url']);
        $container->setParameter('miniature-happiness.two-factor.totp.url_qr', $config['totp']['url_qr']);

        $container->setParameter('miniature-happiness.two-factor.ga.enabled', $config['google_authenticator']['enabled']);
        $container->setParameter('miniature-happiness.two-factor.ga.url', $config['google_authenticator']['url']);
        $container->setParameter('miniature-happiness.two-factor.ga.url_qr', $config['google_authenticator']['url_qr']);

        $container->setParameter('miniature-happiness.two-factor.authy.enabled', $config['authy']['enabled']);
        $container->setParameter('miniature-happiness.two-factor.authy.url', $config['authy']['url']);
    }

    private function setControllers(ContainerBuilder $container, array $config): void
    {
        $container->getDefinition('miniature-happiness.two-factor.controllers.email')
            ->replaceArgument('$templateDisabled', $config['email']['template_disabled'])
            ->replaceArgument('$templateEnabled', $config['email']['template_enabled']);

        $container->getDefinition('miniature-happiness.two-factor.controllers.totp')
            ->replaceArgument('$templateDisabled', $config['totp']['template_disabled'])
            ->replaceArgument('$templateQr', $config['totp']['template_qr'])
            ->replaceArgument('$templateEnabled', $config['totp']['template_enabled']);

        $container->getDefinition('miniature-happiness.two-factor.controllers.ga')
            ->replaceArgument('$templateDisabled', $config['google_authenticator']['template_disabled'])
            ->replaceArgument('$templateQr', $config['google_authenticator']['template_qr'])
            ->replaceArgument('$templateEnabled', $config['google_authenticator']['template_enabled']);

        $container->getDefinition('miniature-happiness.two-factor.controllers.authy')
            ->replaceArgument('$templateDisabled', $config['authy']['template_disabled'])
            ->replaceArgument('$templateEnabled', $config['authy']['template_enabled']);
    }
}