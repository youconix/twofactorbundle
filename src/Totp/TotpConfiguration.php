<?php

declare(strict_types=1);

namespace MiniatureHappiness\TwoFactorBundle\Totp;

if (class_exists('\Scheb\TwoFactorBundle\Model\Totp\TotpConfiguration')) {
    class TotpConfiguration extends \Scheb\TwoFactorBundle\Model\Totp\TotpConfiguration
    {
        public function __construct(string $secret, string $algorithm, int $period, int $digits)
        {
            parent::__construct($secret, $algorithm, $period, $digits);
        }
    }
}
else {
    class TotpConfiguration
    {
        public const ALGORITHM_SHA1 = 'sha1';

        public function __construct(string $secret, string $algorithm, int $period, int $digits)
        {
        }
    }
}